Rails.application.routes.draw do
  devise_for :users, :controllers => {
      sessions: 'users/sessions'
  }
  root 'welcome#index'
  resources :users, only: [:index, :show, :edit, :update, :destroy]
  resources :messages, only: [:create]
  resources :rooms, only: [:create, :index, :show, :edit, :update, :destroy]
  if Rails.env.development? #開発環境の場合
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end
end
