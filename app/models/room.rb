class Room < ApplicationRecord
  validates :name, presence: true, length: { maximum: 20 }
  validates :explain, length: { maximum: 140 }
  has_many :messages, dependent: :destroy
  has_many :entries, dependent: :destroy
end
