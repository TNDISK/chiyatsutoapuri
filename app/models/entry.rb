class Entry < ApplicationRecord
    belongs_to :room
    belongs_to :user

    validates :room_id, uniqueness: { scope: :user_id,
    message: "すでにチャットルームができています" }
end
