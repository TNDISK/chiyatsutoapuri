class ApplicationController < ActionController::Base
  def after_sign_in_path_for(resource)
    if current_user.name == "ゲスト"
      edit_user_path(current_user)
    else
      current_user
    end
  end
end
