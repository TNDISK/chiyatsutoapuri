class RoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :correct_room_user, only: [:show, :edit, :update, :destroy]

  def index
    @entries = current_user.entries
  end

  def show
    @messages = @room.messages.all
    @message = Message.new
    @entries = @room.entries
    # 既読未読機能
    @messagess = @room.messages.where("(read_flg = ?) AND NOT (user_id = ?)", 0, current_user.id)
    if @messagess.present?
      @messagess.each do |m|
        m.update_attributes(read_flg: 1)
      end
    end
  end

  def create
    @room = Room.create #(params_room)
    @entry1 = @room.entries.create(user_id: current_user.id)
    @entry2 = @room.entries.create(params_entry)
    flash[:notice] = "トークルームを作成しました"
    redirect_to "/rooms/#{@room.id}"
  end

  def edit
  end

  def update
    if @room.update(params_room)
      flash[:notice] = "チャット情報を変更しました"
      redirect_to @room
    else
      render 'edit'
    end
  end

  def destroy
    @room.destroy
    flash[:notice] = "チャットルームを削除しました"
    redirect_to rooms_path
  end

  private

    def set_room
      @room = Room.find(params[:id])
    end

    def params_room
      params.require(:room).permit(:name, :explain)
    end

    def params_entry
      params.require(:entry).permit(:user_id, :room_id)
    end

    def correct_room_user
      unless @room.entries.where(user_id: current_user.id).present?
        flash[:alert] = "無効なユーザーです"
        redirect_back(fallback_location: root_path)
      end
    end

end
