class MessagesController < ApplicationController
  before_action :authenticate_user!, only: [:create]
  def create
    if current_user.entries.where(room_id: params[:message][:room_id]).present?
      current_user.messages.create(params_message)
        # redirect_to "/users/#{params[:id].to_i}"
    else
      flash[:alert] = "無効なユーザー"
    end
    redirect_back(fallback_location: root_path)
  end

  private

    def params_message
      params.require(:message).permit(:user_id, :room_id, :content)
    end
end
