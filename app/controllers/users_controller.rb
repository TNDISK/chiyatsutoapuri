class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destory]
  before_action :correct_user, only: [:edit, :update, :destroy]

  def show
    @room = Room.new
    @entry = Entry.new
    @entries = current_user.entries.limit(3) if current_user.id == params[:id].to_i
  end

  def index
    if params[:skill_list].present?
      @users = User.tagged_with(params[:skill_list])
    else
      @users = User.all
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:notice] = "編集が完了しました"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    flash[:notice] = "ユーザーを削除しました"
    redirect_to root_url
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(*key_names)
    end

    def key_names
      [
        :name,
        :self_introduction,
        :age,
        :picture,
        :skill_list,
        :gender_id,
        :birthday,
        :birthday_public,
        :location
      ]
    end

    def correct_user
      @user = User.find(params[:id])
        unless @user.id == current_user.id
          flash[:alert] = "無効なユーザーです"
          redirect_to users_url
        end
    end

end
