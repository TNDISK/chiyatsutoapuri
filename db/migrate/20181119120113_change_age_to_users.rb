class ChangeAgeToUsers < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :age, :integer, default: 0
  end
end
