class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms do |t|
      t.string  :name, default: "新規チャット", null: false
      t.text   :explain
      
      t.timestamps null: false
    end
  end
end
