class ChangeColumnToMessages < ActiveRecord::Migration[5.2]
  def change
    remove_column :messages, :receiver, :integer
  end
end
