class SetDefaultToExpalin < ActiveRecord::Migration[5.2]
  def change
    change_column :rooms, :explain, :string, default: "説明はまだありません"
  end
end
