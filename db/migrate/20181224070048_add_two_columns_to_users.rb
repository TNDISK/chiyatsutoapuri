class AddTwoColumnsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :birthday, :datetime
    add_column :users, :birthday_public, :boolean, default: true
  end
end
