class ChangeColumn < ActiveRecord::Migration[5.2]
  def change
    change_column :rooms, :name, :string, default: "新規チャット", null: true
  end
end
